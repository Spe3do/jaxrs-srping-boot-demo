package hu.dpc.edu.jaxrsdemo.rest;

import hu.dpc.edu.jaxrsdemo.services.HelloService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
@Component
@Api
public class HelloResource {

    private HelloService helloService;

    public HelloResource(HelloService helloService) {
        this.helloService = helloService;
    }

    @ApiOperation("Interesting")
    @Path("/world")
    @Produces("text/plain")
    @GET
    public String helloWorld() {
        return helloService.hello();
    }
}
