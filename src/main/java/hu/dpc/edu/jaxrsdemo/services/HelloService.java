package hu.dpc.edu.jaxrsdemo.services;

import org.springframework.stereotype.Component;

@Component
public class HelloService {
    public String hello() {
        return "Hello World!!!";
    }
}
