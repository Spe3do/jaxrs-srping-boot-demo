package hu.dpc.edu.jaxrsdemo;

import hu.dpc.edu.jaxrsdemo.rest.HelloResource;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("rest")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(HelloResource.class);
        registerSwagger();
    }


    @Value("${server.port}")
    private int port;

    private void registerSwagger() {
        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);

        BeanConfig config = new BeanConfig();
        config.setTitle("Spring boot jaxrs swagger integration");
        config.setSchemes(new String[] {"http", "https"});
        config.setHost("localhost:" + port);
        config.setVersion("v1");
        config.setBasePath("/rest");
        config.setResourcePackage("hu.dpc.edu.jaxrsdemo");
        config.setPrettyPrint(true);
        config.setScan(true);
        // http://localhost:port/v1/swagger.json
    }


}